# Sistema de Prevención de Intrusos Basado en Machine Learning

## Objetivo del Programa
El objetivo de este programa es implementar un Sistema de Prevención de Intrusos (SPI) utilizando técnicas de Machine Learning para detectar tráfico de red intrusivo en tiempo real. Este sistema puede clasificar el tráfico como normal o intrusivo, permitiendo una respuesta proactiva a posibles ciberataques.

## Requisitos Previos
- Python 3.6 o superior
- pip (Python package installer)

## Crear un Entorno Virtual con Python
1. Abre una terminal o línea de comandos.
2. Navega al directorio donde deseas crear el entorno virtual.
3. Ejecuta el siguiente comando para crear un entorno virtual:
    ```sh
    python -m venv venv
    ```
4. Activa el entorno virtual:
    - En Windows:
        ```sh
        .\venv\Scripts\activate
        ```
    - En macOS y Linux:
        ```sh
        source venv/bin/activate
        ```

## Instalación de las Librerías
1. Asegúrate de que el entorno virtual esté activado.
2. Crea un archivo llamado `requirements.txt` con las siguientes dependencias:
    ```txt
    pandas
    scikit-learn
    tensorflow
    flask
    ```
3. Ejecuta el siguiente comando para instalar las librerías:
    ```sh
    pip install -r requirements.txt
    ```

## Ejecución del Programa
1. Asegúrate de que el entorno virtual esté activado y las librerías estén instaladas.
2. Ejecuta el archivo `app.py` para iniciar el servicio web:
    ```sh
    python app.py
    ```

## Ejecución del Programa con POSTMAN
1. Abre POSTMAN.
2. Configura una nueva solicitud POST:
    - **URL**: `http://127.0.0.1:5000/predict`
    - **Método**: `POST`
    - **Headers**: 
        - Key: `Content-Type`
        - Value: `application/json`
    - **Body**: Selecciona la opción `raw` y el tipo `JSON`. Carga el contenido del archivo `data.json`.

## Resultados de la Petición POST

La respuesta del servidor será un JSON que contendrá una predicción basada en los datos enviados. Los posibles resultados son:

- **{"prediction": 0}**: 
  - Indica que el tráfico de red proporcionado se ha clasificado como **no intrusivo**. 
  - Esto significa que el modelo ha determinado que los datos ingresados no representan una amenaza o actividad sospechosa en la red.

- **{"prediction": 1}**: 
  - Indica que el tráfico de red proporcionado se ha clasificado como **intrusivo**.
  - Esto significa que el modelo ha detectado una actividad sospechosa o un posible ciberataque basado en los datos ingresados.


# Objetivo de los Archivos .py

## `preprocessing.py`

El archivo `preprocessing.py` se encarga de la carga, codificación y normalización de los datos. Su objetivo es preparar los datos de entrada para ser utilizados por los modelos de Machine Learning. Las funciones clave incluyen:

- **load_data(url)**: Carga los datos desde un archivo CSV y asigna nombres a las columnas.
- **preprocess_data(df)**: Codifica las características categóricas en valores numéricos y normaliza las características.
- **split_data(X, y)**: Divide los datos en conjuntos de entrenamiento y prueba.

## `model_training.py`

El archivo `model_training.py` contiene las funciones necesarias para entrenar los modelos de Machine Learning. Su objetivo es construir y ajustar los modelos para que puedan realizar predicciones precisas. Las funciones clave incluyen:

- **train_random_forest(X_train, y_train)**: Entrena un modelo de Random Forest con los datos de entrenamiento.
- **train_autoencoder(X_train)**: Entrena un modelo de Autoencoder para la detección de anomalías.
- **evaluate_model(model, X_test, y_test)**: Evalúa el rendimiento del modelo utilizando los datos de prueba y muestra un informe de clasificación.

## `detection.py`

El archivo `detection.py` implementa la lógica para la detección de intrusiones en tiempo real. Su objetivo es combinar las predicciones de los modelos entrenados para determinar si el tráfico de red es intrusivo o no. La función clave incluye:

- **detect_intrusion(rf_model, autoencoder, data, threshold=0.02)**: Combina las predicciones del modelo de Random Forest y el Autoencoder para detectar intrusiones basadas en un umbral de error de reconstrucción.

## `app.py`

El archivo `app.py` configura y ejecuta el servicio web utilizando Flask. Su objetivo es proporcionar una API que permita a los usuarios enviar datos de tráfico de red y recibir una predicción de intrusión. Las funcionalidades clave incluyen:

- **Flask app setup**: Configura la aplicación Flask y define las rutas.
- **@app.route('/predict', methods=['POST'])**: Define el endpoint `/predict` que recibe una solicitud POST con los datos de tráfico de red, procesa los datos utilizando los modelos entrenados y devuelve una predicción en formato JSON.
- **main block**: Inicia el servidor Flask para que esté listo para recibir solicitudes.