import pandas as pd
from sklearn.preprocessing import LabelEncoder, MinMaxScaler
from sklearn.model_selection import train_test_split

def load_data(url):
    columns = ['duration', 'protocol_type', 'service', 'flag', 'src_bytes', 'dst_bytes', 'land', 'wrong_fragment', 'urgent',
               'hot', 'num_failed_logins', 'logged_in', 'num_compromised', 'root_shell', 'su_attempted', 'num_root',
               'num_file_creations', 'num_shells', 'num_access_files', 'num_outbound_cmds', 'is_host_login',
               'is_guest_login', 'count', 'srv_count', 'serror_rate', 'srv_serror_rate', 'rerror_rate', 'srv_rerror_rate',
               'same_srv_rate', 'diff_srv_rate', 'srv_diff_host_rate', 'dst_host_count', 'dst_host_srv_count',
               'dst_host_same_srv_rate', 'dst_host_diff_srv_rate', 'dst_host_same_src_port_rate', 'dst_host_srv_diff_host_rate',
               'dst_host_serror_rate', 'dst_host_srv_serror_rate', 'dst_host_rerror_rate', 'dst_host_srv_rerror_rate', 'label', 'last_column']
    df = pd.read_csv(url, header=None, names=columns)
    df.drop('last_column', axis=1, inplace=True)  # Eliminar la última columna extra
    return df

def preprocess_data(df):
    # Verificar datos originales
    print("Datos originales:")
    print(df.head())
    
    # Codificar características categóricas
    categorical_columns = ['protocol_type', 'service', 'flag']
    label_encoders = {}
    for col in categorical_columns:
        le = LabelEncoder()
        df[col] = le.fit_transform(df[col])
        label_encoders[col] = le
    
    # Verificar datos después de la codificación
    print("Datos después de la codificación:")
    print(df.head())
    
    # Convertir todas las columnas a numéricas (asegurando que no haya strings)
    for col in df.columns:
        if df[col].dtype == 'object':
            print(f"Columna {col} contiene valores no numéricos")
    
    # Normalizar las características
    scaler = MinMaxScaler()
    df[df.columns[:-1]] = scaler.fit_transform(df[df.columns[:-1]])
    
    X = df.drop('label', axis=1)
    y = df['label'].apply(lambda x: 0 if x == 'normal' else 1)
    return X, y, label_encoders, scaler

def split_data(X, y):
    return train_test_split(X, y, test_size=0.3, random_state=42)
