import numpy as np

def detect_intrusion(rf_model, autoencoder, data, threshold=0.02):
    rf_pred = rf_model.predict(data)
    ae_pred = autoencoder.predict(data)
    mse = np.mean(np.power(data - ae_pred, 2))
    ae_pred = 1 if mse > threshold else 0
    final_pred = max(rf_pred[0], ae_pred)
    return final_pred
