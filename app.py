from flask import Flask, request, jsonify
import preprocessing
import model_training
import detection

# Cargar y preprocesar datos
df = preprocessing.load_data("https://raw.githubusercontent.com/defcom17/NSL_KDD/master/KDDTrain+.txt")
X, y, label_encoders, scaler = preprocessing.preprocess_data(df)
X_train, X_test, y_train, y_test = preprocessing.split_data(X, y)

# Entrenar modelos
rf_model = model_training.train_random_forest(X_train, y_train)
autoencoder = model_training.train_autoencoder(X_train)

# Evaluar modelos
model_training.evaluate_model(rf_model, X_test, y_test)

# Crear la aplicación Flask
app = Flask(__name__)

@app.route('/predict', methods=['POST'])
def predict():
    data = request.json['data']
    # Convertir datos de entrada usando los label_encoders y scaler
    for col in label_encoders:
        data[col] = label_encoders[col].transform([data[col]])[0]
    data = scaler.transform([list(data.values())])
    prediction = detection.detect_intrusion(rf_model, autoencoder, data)
    return jsonify({'prediction': int(prediction)})

if __name__ == '__main__':
    app.run(debug=True)
